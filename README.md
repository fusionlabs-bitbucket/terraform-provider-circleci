# Terraform cirleci provider
Version: 0.3.0

This repo contains the `linux_amd64` build of the provider & is inteded to be used on Terraform Cloud.

[Source](https://github.com/mrolla/terraform-provider-circleci)


## Usage

```terraform
terraform {
  required_providers {
    # ...
    circleci = {
      source  = "registry.local/mrolla/circleci"
      version = "0.3.0"
    }
  }
}

provider "circleci" {
  api_token    = var.circleci_token
  vcs_type     = "bitbucket" | "github"
  organization = "<your bitbucket/github organisation>"
}
```

## Intallation (Cloud)
The installation steps below are for terraform cloud.
Note: Only Terraform 0.13+ is supported.

### Configuration per environment strategy

```bash
git submodule add https://bitbucket.org/fusionlabs-bitbucket/terraform-provider-circleci plugins/registry.local/mrolla/circleci/
mkdir -p <env>/terraform.d
ln -s ./plugins <env>/terraform.d/plugins
```

### Monorepo strategy

```bash
git submodule add https://bitbucket.org/fusionlabs-bitbucket/terraform-provider-circleci terraform.d/plugins/registry.local/mrolla/circleci/
```

## Installation (macOS)

If you need to run this provider on your local environment on macOS then:

- [Download the relevant release](https://github.com/mrolla/terraform-provider-circleci/releases) and rename it to `terraform-provider-circleci_v0.3.0`
- Run `chmod +x terraform-provider-circleci_v0.3.0`
- Copy it to both `/Library/Application Support/io.terraform/plugins/registry.local/mrolla/circleci/0.3.0/darwin_amd64` and `~/Library/Application Support/io.terraform/plugins/registry.local/mrolla/circleci/0.3.0/darwin_amd64`
- If your mac complains about security, make sure to allow it to run
